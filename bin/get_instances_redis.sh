function get_hosts {
    local REDISHOST=${1:-redis}
	hosts=$(redis-cli -h "${REDISHOST}" --raw smembers saas:instances);
}
