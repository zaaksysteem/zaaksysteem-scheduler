# Zaaksysteem Scheduler

## BUILD

```
docker build -t registry.gitlab.com/zaaksysteem/zaaksysteem-scheduler .
```

## Run

```
docker run registry.gitlab.com/zaaksysteem/zaaksysteem-scheduler
```

## Push

```
docker push registry.gitlab.com/zaaksysteem/zaaksysteem-scheduler
```